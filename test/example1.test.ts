const a = [2, -7, 30, 5, -88, 199, 20, -1, 33, 64];

const sortIntF = (a: number[], order = "asc") => (order === "desc" ? a.sort((i, j) => (i === j ? 0 : i - j < 0 ? 1 : -1)) : a.sort((i, j) => i - j));

describe("should produce right answers", () => {
  test("should sort the array in ascending order", () => {
    expect(sortIntF(a)).toStrictEqual([-88, -7, -1, 2, 5, 20, 30, 33, 64, 199]);
  });
});
