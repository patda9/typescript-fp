export const a1 = [1, 2, 3, 4, 5];
export const a2 = [10, -8, 16, -72, 33, 64, 0, 2, 22, 32, -44];
export const a3 = ["Honda", "Suzuki", "Toyota", "Subaru", "Isuzu", "Nissan", "Mazda"];
export const a4 = [1, 1, 3, -4, -4, 5, 5, 4, 0];

export const mapExample1 = a1.map(n => n * n);
export const mapExample2 = a1.map((n, i) => ({ index: i, value: n, isEven: n % 2 === 0 }));

export const filterExample1 = a1.filter(n => n % 2 === 0);
export const filterExample2 = a3.filter(s => s.toLowerCase().startsWith("s"));

export const findExample1 = a1.find(n => n % 2 === 0);

export const sortExample1 = a2.sort();
export const sortExample2 = a2.sort((i, j) => (i === j ? 0 : i - j < 0 ? 1 : -1));

export const distinctExample = a4.reduce((acc: number[], n) => (acc.includes(n) ? acc : acc.concat(n)), []);
export const partitionExample = a4.reduce(
  (acc: { odds: number[]; evens: number[] }, n) => {
    if (n % 2 === 0) return { ...acc, evens: acc.evens.concat(n) };

    return { ...acc, odds: acc.odds.concat(n) };
  },
  { odds: [], evens: [] }
);
export const minExample = a4.slice(1).reduce((min: number, n) => (n < min ? n : min), a4[0]);
export const collectExample = a4.reduce(
  (acc: { value: number; isEven: boolean }[], n) => (n > 0 ? acc.concat({ value: n, isEven: n % 2 === 0 }) : acc),
  []
);
export const toMapExample = ["zero", "one", "two", "three", "four"].reduce((acc: { [key: string]: number }, ns, i) => {
  acc[ns] = i;

  return acc;
}, {});
