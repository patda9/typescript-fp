# Functional Programming in TS

## Given

```
const a: number[] = [45, -60, 4, -30, 91, 82, 93, -5, 38, -18, 0]
const b: number[] = [1, 2, 3, [4, [5, 6]]]
```

## Map

Use to transform every element of type A to type B

### Examples

```
a.map(n => n * n)
> [2025, 3600, 16, 900, 8281, 6724, 8649, 25, 1444, 324, 0]


a.map(n => ({ value: n, isEven: n % 2 === 0 }))
> [
    {value: 45, isEven: false},
    {value: -60, isEven: true},
    {value: 4, isEven: true},
    {value: -30, isEven: true},
    {value: 91, isEven: false},
    {value: 82, isEven: true},
    {value: 93, isEven: false},
    {value: -5, isEven: false},
    {value: 38, isEven: true},
    {value: -18, isEven: true},
    {value: 0, isEven: true},
  ]


const toRatioOf100 = (n: number) => n / 100
a.map(toRatioOf100)
> [0.45, -0.6, 0.04, -0.3, 0.91, 0.82, 0.93, -0.05, 0.38, -0.18, 0]
```

## Flat map

Like map but also flatten top level of iterable oject

### Example

```
b.flatMap(n => n)
> [1, 2, 3, 4, [5, 6]]
```

## Filter

Use to collect elements match with condition

### Example

```
a.filter(n => n >= 0)
> [45, 4, 91, 82, 93, 38, 0]

a.filter(n => n > 100)
> []
```

## Find

Return first element matches with condition

### Example

```
a.find(n => n % 2 === 0)
> -60

a.find(n => n > 100)
> undefined
```

## Sort

> Caution: This function mutates iterable object state if not making shallow copy of old one

### Example

```
const a1 = [...a]
const a2 = [...a]

a1.sort() // sort without shallow copy
> [-18, -30, -5, -60, 0, 38, 4, 45, 82, 91, 93]
a1
> [-18, -30, -5, -60, 0, 38, 4, 45, 82, 91, 93]

[...a2].sort() // make a shallow copy of a then start sorting
> [-18, -30, -5, -60, 0, 38, 4, 45, 82, 91, 93]
a2
> [45, -60, 4, -30, 91, 82, 93, -5, 38, -18, 0]

["u", "o", "i", "e", "a"].sort() // string sorting
> ["a", "e", "i", "o", "u"]

// custom sort
["Mazda", "Toyota", "Nissan", "Honda", "Subaru", "Mitsubish"]
  .sort((i: string, j: string) => {
      if (i.startsWith("M")) {
        if (!j.startsWith("M")) return -1
        return i.localeCompare(j)
      }

      if (j.startsWith("M")) return 1
      return i.localeCompare(j)
  })
> ["Mazda", "Mitsubish", "Honda", "Nissan", "Subaru", "Toyota"]
```

## Slice

Return element at `start` index to

### Example

```
a.slice(1)                                  // [start => 1, end => a.length)
> [-60, 4, -30, 91, 82, 93, -5, 38, -18, 0]

a.slice(1, 4)                               // [start => 1, end => 4)
> [-60, 4, -30]

a.slice(-2)                                 // [start => a.length - 2, end => a.length)
> [-18, 0]

```

## Reduce (Fold)

```
a.reduce((acc: number, n) => acc + n, 0)
> 240

[1, 1, 2, 2, 3, 4, 5, 5, 5]
  .reduce(
    (acc: number, n) => acc.includes(n) ? acc : acc.concat(n),
    []
  )
> 240

a.reduce(
  (acc: string[], n) =>
    n % 2 === 0
      ? acc.concat((n * 2).toString())
      : acc,
  []
)
> ["-120", "8", "-60", "164", "76", "-36", "0"]
```
