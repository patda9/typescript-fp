# typescript-fp

## Initial TypeScript project with NPM 

### Create Javascript project using NPM

1. 
    ```
    npm init -y
    ```
2. 
    ```
    npm i -g typescript
    ```
3. Replace `package.json` with
    ```
    {
      "name": "typescript-fp",
      "version": "1.0.0",
      "main": "index.js",
      "directories": {
        "test": "test"
      },
      "dependencies": {},
      "devDependencies": {
        "@types/jest": "27.0.2",
        "jest": "27.3.1",
        "ts-jest": "27.0.7",
        "ts-node": "10.4.0",
        "typescript": "4.4.4"
      },
      "scripts": {
        "start": "tsc && node ./dist/index.js",
        "test": "jest --config ./jest.config.js",
        "testMatch": "**/test/**/*.test.(js|ts)"
      }
    }
    ```
4. 
      ```
      npm install
      ```

### Creating Typescript Configuration

1.
    ```
    tsc --init
    ```
1. Replace `tsconfig.json` with
    ```
    {
      "compilerOptions": {
        "outDir": "./dist",
        "target": "es5",
        "lib": ["es2015", "es2019", "es5", "es6", "DOM"],
        "module": "commonjs",
        "strict": true,
        "esModuleInterop": true,
        "skipLibCheck": true,
        "forceConsistentCasingInFileNames": true,
      }
    }
    ```
